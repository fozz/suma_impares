print("Este programa sumara los números impares positivos que les introduzcas")

def VariableA():
    while True:
        try:
            a = int(input("Dame un número entero no negativo e impar: "))
            if a > 0 and a % 2 != 0:
                # Al dividir si el resto no es igual a 0 quiere decir que el número introducido es impar
                return a
            else:
                print("Introduce un número impar y no negativo, por favor.")
        except ValueError:
            print("Por favor, introduce un número entero válido.")

def VariableB():
    while True:
        try:
            b = int(input("Dame otro número entero no negativo e impar: "))
            if b > 0 and b % 2 != 0:
                return b
            else:
                print("Introduce un número impar y no negativo, por favor.")
        except ValueError:
            print("Por favor, introduce un número entero válido.")

def suma(a, b):
    resultado = a + b
    return resultado

def iniciar_programa():
    a = VariableA()
    b = VariableB()
    resultado = suma(a, b)
    print("La suma de los números es:", resultado)

if __name__ == "__main__":
    iniciar_programa()